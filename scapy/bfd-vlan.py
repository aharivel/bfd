 from scapy.all import *

def create_bfd_packet_with_vlan(destination_mac, vlan_id):
    # BFD Header Fields
    bfd_version = 1  # BFD version
    bfd_diag = 0  # Diagnostic field (no diagnostic)
    bfd_state = 3  # State: Up
    bfd_flags = 0  # Flags field (no flags set)
    bfd_detect_mult = 3  # Detection multiplier (arbitrary value)
    bfd_len = 24  # Length of the BFD packet
    bfd_my_discr = 1  # My discriminator
    bfd_your_discr = 0  # Your discriminator
    bfd_desired_min_tx_interval = 1_000_000  # Desired min TX interval (1 second)
    bfd_required_min_rx_interval = 1_000_000  # Required min RX interval (1 second)
    bfd_required_min_echo_interval = 0  # No echo

    # BFD Control Packet (as bytes)
    bfd_packet = struct.pack(
        "!BBHBBHHIII",  # Updated format string for packing
        (bfd_version << 5) | bfd_diag,  # Version and diagnostic
        bfd_state,  # State
        bfd_flags,  # Flags
        bfd_detect_mult,  # Detection multiplier
        bfd_len,  # Length
        bfd_my_discr,  # My discriminator
        bfd_your_discr,  # Your discriminator
        bfd_desired_min_tx_interval,  # Desired min TX interval
        bfd_required_min_rx_interval,  # Required min RX interval
        bfd_required_min_echo_interval  # Required min echo interval
    )

    # Modify the IP address accordingly
    ip_layer = IP(src="192.168.1.1", dst="192.168.1.2")
    udp_layer = UDP(sport=49152, dport=3784)
    raw_layer = Raw(load=bfd_packet)

    # Create the complete packet with VLAN tagging
    full_packet = Ether(dst=destination_mac) / Dot1Q(vlan=vlan_id) / ip_layer / udp_layer / raw_layer

    return full_packet

# Create a BFD packet with VLAN tagging
# Modify the MAC Address and the VLAN tag accordingly
bfd_packet_with_vlan = create_bfd_packet_with_vlan("30:3e:a7:0b:f2:b1", 100)

# Display the packet structure
bfd_packet_with_vlan.show()

# send 'count' packets to iface
sendp(bfd_packet_with_vlan, iface="eno12409np1", count=100)

