from scapy.all import *

def create_bfd_ipv6_packet(destination_mac, source_ipv6, dest_ipv6):
    # BFD Header Fields
    bfd_version = 1  # BFD version
    bfd_diag = 0  # Diagnostic field (no diagnostic)
    bfd_state = 3  # State: Up
    bfd_flags = 0  # Flags field (no flags set)
    bfd_detect_mult = 3  # Detection multiplier (arbitrary value)
    bfd_len = 24  # Length of the BFD packet
    bfd_my_discr = 1  # My discriminator
    bfd_your_discr = 0  # Your discriminator
    bfd_desired_min_tx_interval = 1_000_000  # Desired min TX interval (1 second)
    bfd_required_min_rx_interval = 1_000_000  # Required min RX interval (1 second)
    bfd_required_min_echo_interval = 0  # No echo

    # BFD Control Packet (as bytes)
    bfd_packet = struct.pack(
        "!BBHBBHHIII",  # Updated format string for packing
        (bfd_version << 5) | bfd_diag,  # Version and diagnostic
        bfd_state,  # State
        bfd_flags,  # Flags
        bfd_detect_mult,  # Detection multiplier
        bfd_len,  # Length
        bfd_my_discr,  # My discriminator
        bfd_your_discr,  # Your discriminator
        bfd_desired_min_tx_interval,  # Desired min TX interval
        bfd_required_min_rx_interval,  # Required min RX interval
        bfd_required_min_echo_interval,  # Required min echo interval
    )

    ipv6_layer = IPv6(src=source_ipv6, dst=dest_ipv6)  # Example IPv6 addresses
    udp_layer = UDP(sport=49152, dport=3784)  # Default BFD port
    raw_layer = Raw(load=bfd_packet)

    # Combine layers to form the complete packet
    full_packet = Ether(dst=destination_mac) / ipv6_layer / udp_layer / raw_layer

    return full_packet

# Create a BFD packet for IPv6
bfd_ipv6_packet = create_bfd_ipv6_packet("30:3e:a7:0b:f2:b1", "2001:db8::1", "2001:db8::2")

# Display the packet structure
bfd_ipv6_packet.show()

# send 'count' packets to iface
sendp(bfd_ipv6_packet, iface="eno12409np1", count=100)

