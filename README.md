# BFD

This project aims to gather all BFD (Bidirectional Forwarding Detection) code snippets and tests configuration.

## Scapy

- 'bfd-ipv4.py' aims to generate a ipv4 BFD packet example and send it to an interface.
- 'bfd-ipv6.py' aims to generate a ipv6 BFD packet example and send it to an interface.
- 'bfd-vlan.py' aims to generate a ipv4 BFD packet with VLAN example and send it to an interface.

## DPDK

### examples/bfd_filtering:
This application shows a simple usage of the rte_flow API for filtering BFD
(Bidirectional Forwarding Detection) paquet to specific target queue, while
sending all the rest of the packets to other queue.

Build:
meson build [...] -Dexamples=bfd_filtering

Run:
./build/examples/dpdk-bfd_filtering -l 2 -n 1

## OVS
This branch of OVS (openvswitch) is the development branch for adding BFD.
